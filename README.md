# GNUKhata website files

Source code of https://gnukhata.org

- Build Requirements

1. hugo
2. git

# Website

### Local development

```sh
git clone https://gitlab.com/gnukhata/gnukhata.gitlab.io
cd gnukhata.gitlab.io
# live preview (includes draft pages, without -D flag hides draft pages)
hugo server -D
```

### Add meeting notes

Run the command `hugo new weekly-meetings/dd-mm-yyyy.md` (replace dd-mm-yyyy with the relevant date)

A more simple way is use this shell script (for linux)

```sh
# set the EDITOR env variable to your choice of editor
# creates a post named current date dd-mm-yyyy.md
# we can optionally specify custom file name as first parameter and
# opens the newly created file in $EDITOR
./add-meet.sh
```

Commit the changes and push to the repo

> Template for meeting notes is in `archetypes/weekly-meetings.md`

### Adding blog post

Run the command `hugo new blog/your-post-title.md`

> Template for blog is in `archetypes/blog.md`

### Weekly Meeting Link

The meeting link is located in `content/english/meet.md`

### Swagger UI

This repo also builds the swagger ui which is located at `https://gnukhata.org/api` using the gitlab ci. Refer the `.gitab-ci.yml` file for more info

# Docs

- GNUKhata's documentation is created using `mkdocs`
- Docs are built & deployed via the cicd pipeline.

Install dependencies

```sh
pip install mkdocs mkdocs-material
```
Edit the docs locally with live preview:

```sh
cd docs/
mkdocs serve
```

### Useful links

- [Hugo website](gohugo.io/)
- [Hugo Tutorial (youtube playlist)](https://www.youtube.com/playlist?list=PLLAZ4kZ9dFpOnyRlyS-liKL5ReHDcj4G3)
