---
title: "Feedback on GNUKhata"
date: 2022-11-09T12:32:39+05:30
draft: false
tags: ["release"]
---

Dear GNUKhata community,

Your feedback helps GNUKhata team prioritize tasks which result in better user experience. Please spare a few minutes & fill out the below form & help improve GNUKhata!

> The cryptpad feedback page below may take a minute to load. Please be patient.

<iframe width="100%" height="1000" src="https://cryptpad.fr/form/#/2/form/view/yBO-T8C-ZjD16o5Boy5MQVWjEDuNNZ7Hq7ql6qVOBP8/embed/"></iframe>
