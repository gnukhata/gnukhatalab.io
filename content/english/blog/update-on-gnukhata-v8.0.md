---
title: "Update on the upcoming GNUkhata v8.0"
date: 2022-10-12T10:09:29+05:30
draft: false
---

Dear GNUKhata users & Community,

We are very excited to share some information on the upcoming version of GNUKhata, Which is v8.0!

This release is a result of 18 months of work, Which is an entire re-write of the older user interface. The newer user interface is a mobile & user friendly. This release also brings major changes to user authentication, a new dashboard look, new reports UI & much more!

This release will not have GST functionality. However, You could also have a sneak peak at GST section, which is work in progress.

The alpha version of v8.0 will be made available soon. So, Stay tuned!
