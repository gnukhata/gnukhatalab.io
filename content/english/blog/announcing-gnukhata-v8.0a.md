---
title: "Announcing GNUkhata v8.0 Alpha"
date: 2022-11-06T10:43:56+05:30
draft: false
tags: ["release"]
---

Dear GNUKhata users & Community,

We are very excited to announce the alpha version of the next major version of GNUKhata, which is v8.0a.

This release is a result of 18 months of work, which is an entire re-write of the older user interface. This newer user interface is a mobile & user friendly. This release also brings major changes to user authentication, a new dashboard look, new reports UI & much more!

This release will not have GST functionality. However, you could have a sneak peak at GST section, which is work in progress.

We invite you to test this version & submit feedback, as it adds a lot of value to the upcoming beta & stable release!

> Installation instructions can be found [here](/install)
