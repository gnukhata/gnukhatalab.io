---
title: "FAQ"
date: 2023-05-04T14:30:52+05:30
draft: false
outputs:
  - html
  - amp
  - json
---

## What is GNUKhata?

GNUKhata is a free and open-source accounting and inventory management software designed for small and medium-sized businesses. It allows you to manage your financial transactions, inventory, and customer information in one place.

## How do I get started with GNUKhata?

To get started with GNUKhata, you can download the software from the official website and install it on your computer. Once installed, you can create a new company and start setting up your accounts and inventory.

## Do I need any accounting or inventory management experience to use GNUKhata?

No, you do not need any accounting or inventory management experience to use GNUKhata. The software is designed to be user-friendly and intuitive, with step-by-step guides to help you set up your accounts and inventory.

## Can I import my existing accounting data into GNUKhata?

Yes, you can import your existing accounting data into GNUKhata. The software supports import of data in CSV, TSV, and Excel formats.

## How do I create a new company in GNUKhata?

To create a new company in GNUKhata, go to the Home screen and click on "Create Company". Then enter the details of your company, including the name, address, and financial year.

## How do I set up my accounts in GNUKhata?

To set up your accounts in GNUKhata, go to the Accounting module and click on "Chart of Accounts". Then click on "Add New Account" and enter the details of the account, including the account name, type, and opening balance.

## How do I add inventory items in GNUKhata?

To add inventory items in GNUKhata, go to the Inventory module and click on "Items". Then click on "Add New Item" and enter the details of the item, including the item name, description, and price.

## Can I customize the invoices and other financial documents in GNUKhata?

Yes, you can customize the invoices and other financial documents in GNUKhata. The software allows you to add your company logo and customize the layout and design of the documents.

## How do I generate financial reports in GNUKhata?

To generate financial reports in GNUKhata, go to the Accounting module and click on "Reports". Then select the report you want to generate, such as the Trial Balance or Profit & Loss statement, and select the date range for the report.

## Is there any support available for GNUKhata users?

Yes, there is a community forum where GNUKhata users can ask questions and get help from other users and developers. There are also video tutorials and documentation available on the official website.

## How do I create an invoice in GNUKhata?

To create an invoice in GNUKhata, go to the Sales module and click on "New Invoice". Enter the customer details, item details, and other relevant information. Save the invoice, and it will be added to the sales ledger.

## How can I generate a purchase order in GNUKhata?

To generate a purchase order in GNUKhata, go to the Purchase module and click on "New Purchase Order". Enter the supplier details, item details, and other relevant information. Save the purchase order, and it will be added to the purchase ledger.

## Can I set up automatic bank feeds in GNUKhata?

Yes, GNUKhata supports automatic bank feeds, allowing you to import transactions from your bank account directly into GNUKhata. To set up automatic bank feeds, go to the Bank module and click on "Connect Bank". Follow the on-screen instructions to connect your bank account.

## How do I record a payment in GNUKhata?

To record a payment in GNUKhata, go to the Payments module and click on "New Payment". Enter the details of the payment, including the payee, amount, and payment method. Save the payment, and it will be added to the relevant ledger.

## Can I generate financial reports in GNUKhata?

Yes, GNUKhata allows you to generate a range of financial reports, including profit and loss statements, balance sheets, and cash flow statements. To generate a report, go to the Reports module and select the report type.

## How do I reconcile bank transactions in GNUKhata?

To reconcile bank transactions in GNUKhata, go to the Bank module and select the relevant bank account. Click on "Reconcile" and enter the ending balance and the date of the bank statement. Match the transactions in GNUKhata with the transactions on the bank statement, and mark them as reconciled.

## Can I set up recurring invoices in GNUKhata?

Yes, GNUKhata supports recurring invoices, allowing you to create and send invoices automatically at specified intervals. To set up a recurring invoice, go to the Sales module and click on "New Invoice". Click on "Recurring Invoice" and enter the details of the invoice and the recurring frequency.

## How do I add a new customer in GNUKhata?

To add a new customer in GNUKhata, go to the Sales module and click on "New Customer". Enter the customer details, including the name, address, and contact information. Save the customer, and they will be added to the customer list.

## How can I track expenses in GNUKhata?

To track expenses in GNUKhata, go to the Expenses module and click on "New Expense". Enter the details of the expense, including the payee, amount, and category. Save the expense, and it will be added to the expense ledger.

## Can I customize the invoice template in GNUKhata?

Yes, GNUKhata allows you to customize the invoice template to match your branding and design preferences. To customize the template, go to the Settings module and click on "Invoice Settings". Click on "Customize Template" and use the drag-and-drop editor to make changes to the template.

## How do I create a new ledger in GNUKhata?

To create a new ledger in GNUKhata, go to the Accounts module and click on "New Ledger". Enter the name of the ledger and select the ledger type (e.g. expense, income, asset, liability). Save the ledger, and it will be added to the ledger list.

## How can I create a journal entry in GNUKhata?

To create a journal entry in GNUKhata, go to the Accounts module and click on "New Journal Entry". Enter the debit and credit amounts for each account and include a brief description of the entry. Save the journal entry, and it will be added to the journal list.

## Can I import ledger data from an external file into GNUKhata?

Yes, GNUKhata supports importing ledger data from external files, including CSV and Excel files. To import ledger data, go to the Accounts module and click on "Import Ledger". Follow the on-screen instructions to select the file and map the columns to the appropriate ledger fields.

## How do I generate a Trial Balance report in GNUKhata?

To generate a Trial Balance report in GNUKhata, go to the Reports module and select "Trial Balance". Choose the date range and click on "Generate Report". The Trial Balance report will show the balances of all the ledgers in the selected date range.

## Can I filter the Trial Balance report by ledger type or category?

Yes, GNUKhata allows you to filter the Trial Balance report by ledger type or category. Click on "Advanced Options" and select the desired filter criteria.

## How can I generate a Profit & Loss statement in GNUKhata?

To generate a Profit & Loss statement in GNUKhata, go to the Reports module and select "Profit & Loss". Choose the date range and click on "Generate Report". The Profit & Loss statement will show the revenue, expenses, and net income for the selected date range.

## Can I export financial reports from GNUKhata?

Yes, GNUKhata allows you to export financial reports to various formats, including PDF, CSV, and Excel. Click on "Export" and select the desired format.

## How do I customize the chart of accounts in GNUKhata?

To customize the chart of accounts in GNUKhata, go to the Settings module and click on "Chart of Accounts". Use the drag-and-drop interface to reorder the ledgers and modify the ledger types and categories.

## Can I create sub-ledgers in GNUKhata?

Yes, GNUKhata allows you to create sub-ledgers, allowing you to track transactions at a more detailed level. To create a sub-ledger, go to the Accounts module and click on "New Sub-Ledger". Enter the name and select the parent ledger, and save the sub-ledger.

## How do I transfer funds between accounts in GNUKhata?

To transfer funds between accounts in GNUKhata, go to the Accounts module and click on "New Transfer". Enter the details of the transfer, including the accounts involved and the amount. Save the transfer, and it will be added to the ledger list.

## How do I create a godown in GNUKhata?

To create a godown in GNUKhata, go to the Inventory module and click on "Godowns". Then click on "Add New Godown" and enter the name, address, and other relevant details for the godown.

## Can I assign inventory items to specific godowns in GNUKhata?

Yes, you can assign inventory items to specific godowns in GNUKhata. To do so, go to the Inventory module, click on "Items", select the item you want to assign to a godown, and click on the "Edit" button. Then select the godown from the drop-down menu and save the changes.

## How do I transfer inventory items between godowns in GNUKhata?

To transfer inventory items between godowns in GNUKhata, go to the Inventory module, click on "Stock Transfers", and click on "Add New Stock Transfer". Then select the source and destination godowns, select the items to be transferred, and enter the quantity. Save the transfer, and the inventory items will be moved from one godown to another.

## How do I view the inventory levels of a particular godown in GNUKhata?

To view the inventory levels of a particular godown in GNUKhata, go to the Inventory module, click on "Godowns", select the godown you want to view, and click on the "View" button. You can view the quantity and value of each item in that godown, as well as the total inventory value.

## Can I set reorder levels for inventory items in specific godowns in GNUKhata?

Yes, you can set reorder levels for inventory items in specific godowns in GNUKhata. To do so, go to the Inventory module, click on "Items", select the item you want to set reorder levels for, and click on the "Edit" button. Then enter the minimum and maximum inventory levels for the godown, and save the changes.

## How do I generate a report of inventory levels in a specific godown in GNUKhata?

To generate a report of inventory levels in a specific godown in GNUKhata, go to the Inventory module, click on "Reports", and select "Godown Stock Report". Then select the godown you want to generate the report for, and click on "Generate Report". The report will display the quantity and value of each item in that godown.

## How do I track inventory movements between godowns in GNUKhata?

To track inventory movements between godowns in GNUKhata, go to the Inventory module, click on "Stock Transfers", and select the transfer you want to view. You can view the details of the transfer, including the source and destination godowns, the items transferred, and the quantity.

## Can I set access permissions for specific godowns in GNUKhata?

Yes, you can set access permissions for specific godowns in GNUKhata. To do so, go to the Settings module, click on "Roles & Permissions", and select the role you want to set permissions for. Then select the godown you want to set permissions for, and select the appropriate permissions for that role.

## Can I generate a balance sheet in GNUKhata?

Yes, you can generate a balance sheet in GNUKhata. Go to the Accounting module and click on "Reports". Then select "Balance Sheet" and choose the date range for the report.

## How often should I generate a balance sheet in GNUKhata?

It is recommended to generate a balance sheet at the end of each accounting period, such as monthly, quarterly, or annually.

## What information is required to generate a balance sheet in GNUKhata?

To generate a balance sheet in GNUKhata, you need to have your accounts and transactions entered correctly in the software. Make sure to reconcile all your accounts before generating the balance sheet.

## How do I interpret the balance sheet generated by GNUKhata?

The balance sheet generated by GNUKhata provides information on your company's assets, liabilities, and equity. The total assets should equal the total liabilities and equity, indicating a balanced financial position.

## Can I customize the balance sheet format in GNUKhata?

Yes, you can customize the balance sheet format in GNUKhata. Go to the Accounting module and click on "Reports". Then select "Balance Sheet" and click on "Settings" to customize the format and layout of the report.

## Can I export the balance sheet generated by GNUKhata to other formats?

Yes, you can export the balance sheet generated by GNUKhata to other formats such as PDF or Excel. Go to the Accounting module and click on "Reports". Then select "Balance Sheet" and click on "Export" to choose the desired format
