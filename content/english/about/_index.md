---
title: "About"
date: 2021-09-10T21:20:16+05:30
draft: false
---

GNUKhata is free and open source software (FOSS) for financial accounting (FA) and was initiated to cater to the needs of Small and Medium scale (SME) entreprises, traders, startups and Non-Governmental Organisastions (NGO). GNUKhata was initiated in India, in 2009. Thus it has been enhanced to support specific compliance and return requirements of the GST (tax) records and returns requirements in India. GNUKhata features adequate inventory management support which is integral to financial accounting needs for many businesses.

The project was started with a vision to provide cost effective open source accounting and inventory software to ease the business needs of technically under-served.

<!-- GNUKhata was initiated by Mr. Krishnakant Mane, a Masters in Computer Science and an avid free software advocate, in 2009 with a help of a modest team of programmers. The project was started with a vision to provide an extremely cost effective open source accounting and inventory software to ease the business of technically under-served. This implied that the software had a very short learning curve and catered to the need of small and medium scale traders, startups and even NGOs. -->

 <!-- Digital Freedom Foundation, the NGO which sustained and accelerated the development of GNUKhata has been advocating this very mission of making a commercially viable software for the said category of enterprises and communities. Thus the mission to digitally empower their business, which big IT companies tend to ignore has been at the forefront in GNUKhata's progress. -->

Ever since the project's inception, the mission has been to replace costly proprietary programs which were any ways out of reach for small service providers, retail shops and startups.The major challenge in this mission was to develop a user experience model that suits those users who are not well-versed with digital technology and don't have time to go through complex screens to get their work done. This meant that a lot of features needed automation. GNUKhata now in it's version 5 series has so far gone a long way to achieve this mission, thanks to funding from various government institutes and now through Accion Labs PVT. LTD. It's latest version, namely 5.25 has particularly become popular due to the very automation features with GST compliance.

As a part of this entire vision, the GNUKhata team also intends to train rural and semi urben youth in book keeping. This way they can get self employed by means of providing GST compliance service on a doorstep model to small and medium enterprises in their areas or beyond.

## Team

GNUKhata is being actively developed at [Accion Labs Pvt Ltd](https://www.accionlabs.com/). The team is led by [Dr. Nagarjuna G](https://www.gnowledge.org/) & [Bardwaj VK](https://gitlab.com/bardwaj).

- [Ambady Anand S](https://gitlab.com/bady) (Developer)
- [Kannan V M](https://gitlab.com/kannanvm) (Developer)
- [Priyanka](https://gitlab.com/priyanka1992) (Developer)

## Active Contributors

- [rest2e3](https://gitlab.com/Rest_Bookeeping) (GST Domain knowledge, UI/UX Suggestions, App testing, Community support)

## Former contributers

- [Sai Karthik](https://gitlab.com/kskarthik) (Developer)
- [Survesh VRL](https://gitlab.com/123survesh) (Developer)
- Mr. Krishnakant Mane (Founder & Project Leader) kk@dff.org.in
- Mr. Abhijith Balan (Project Manger) abhijith@dff.org.in
- Ms. Prajkta Patkar (Lead Developer & Chief project-cordinator) prajakta@dff.org.in
- Ark Arjun (Logo designer) arkarjun@gmail.com
- Saloni Vinod Mehta (Tutorial Voice Over) saloni.mehta@sakec.ac.in
- Freya Kotak (Tutorial Voice Over) freya.kotak@sakec.ac.in
- CA Arun Kelkar (Former Domain Consultant) arunkelkar@dff.org.in

## Historical Contributors

Abhishek Alai,Aditi Joshi ,Aditi Patil,Aditya Shukla, Akhil KP Dasan, Akshay Puradkar,Amit Chaugule, Ankita Mahadik, Ankita Shanbhag,Anusha Kadambala,Aparna Kokirkar, Ashutosh Paralikar,Ashwini Jogdand,Ashwini Shinde , Bhavesh Bawadhane,Bhushan Patil,Dinesh Sutar, Ganesh Mashram,Girish Mane ,Ishan Masdekar,Jainam Doshi,Kanchan Kayande, Karan Kamdar,Mohd. Talha Pawaty,Namita Dubey,Navin Karkera, Nirali Dodhia, Nishigandha Bichkar,Nishit Jhakharia,Nitesh Chaughule, Nutan Nivate, Payal Mamidwar,Parabjyot Singh,Parthavi Hora ,Poonam Dhane, Pornima Kolte,Prasad Shegokar,Praveen Arimbrathodiyil,Pravin Dake, Priyali Mhatre,Priyanka Prabhavale,Priyanka Tawde,Radhika Vadegaonkar,Rahul Chaurasiya ,Ravindra Lakal,Reshma Bhatawadekar,Rohan Khairnar,Rohini Baraskar,Ruchika Pai,Rudresh Dongre, Rupali Badguja,Rupali Markad,Sachin Patil,Sadhana Bagal,Sakshi Agrawal,Sayali Yawle,Sanket Kolnoorkar ,Snehal Nighut, Shruti Surve,Shweta Pawar,Sonal Chaudhari,Sushila Sharma,Sundeep Padmanabh, Surya Siji,Trupti Kini, Ujwala Pawde,Uma Mahesh , Vaishali Kamble,Vanita Rajpurohit,Vaibhav Kurhe,Vasudha Kadge, Vinayak Kusumkar and Vishakha Divekar.
