---
title: "Contact"
description: "this is a meta description"

office:
  title: "Get Involved"
  mobile: "0124857985320"
  email: "demo@email.com"
  location: "Dhaka, Bangladedsh"
  content: ""

# opennig hour
opennig_hour:
  title: "Opening Hours"
  day_time:
    - "Monday: 9:00 – 19:00"
    - "Tuesday: 9:00 – 19:00"
    - "Wednesday: 9:00 – 19:00"
    - "Thursday: 9:00 – 19:00"
    - "Friday: 9:00 – 19:00"
    - "Saturday: 9:00 – 19:00"
    - "sunday: 9:00 – 19:00"

draft: false
---
