---
title: "Installation Instructions"
date: 2022-10-03T23:10:04+05:30
draft: false
---

> You can also try & evaluate GNUKhata online from the [demo server](https://gnukhata.gitlab.io/gkapp)

In order to install & run gnukhata on your computer, You need to follow the some steps:

## 1. Installing docker & docker compose

### Linux:

Install docker & docker-compose programs from your package manager

```
# For ubuntu/debian based distributions

sudo apt install docker.io docker-compose
```

### Windows & Mac:

Head over to https://www.docker.com & download the installer & follow the install Instructions

## 2. Setup & Launching the app

> Create a folder named `gnukhata` & Download the docker compose file in that folder: [Click to Download](https://gitlab.com/gnukhata/build/-/raw/master/docker-compose.yml?inline=false)

> These below instuctions are also same for windows users, except that they don't need to prefix the word `sudo` in the command

> All the commands below expect you execute them from the `gnukhata` folder

```
# cd into the directory where the file is downloaded
# Run the command below

sudo docker-compose up -d

# to stop gnukhata, run below command in the same directory

sudo docker-compose down
```

You can access gkapp (web UI) inside your web browser at url [http://localhost:2020](http://localhost:2020) & gkcore (REST API Server) [http://localhost:6543](http://localhost:6543) respectively

> In case of issues in the installation process, Please join our [support groups](/get-involved) & we will try to help you out

## Upgrading to Latest versions

### Linux / Mac:

```
# In your terminal, navigate to the folder where the `docker-compose.yaml` file is located and
# run these commands in order

sudo docker-compose down
sudo docker-compose pull
sudo docker-compose up -d
```

### Windows:

```
# In your terminal, navigate to the folder where the `docker-compose.yaml` file is located
# run these commands in order

docker-compose down
docker-compose pull
docker-compose up -d
```

## Database dump

You can take a snapshot of your entire database to a file with this command. It creates a file named `gkdump.sql` in the same directory.

```
docker-compose exec db pg_dump -U gkadmin -d gkdata > gkdump.sql
```

Restore the database

```
# delete old db
docker-compose down -v —remove-orphans

#  bring up the containers
docker-compose up -d

# copy the sql file to database container
docker cp gkdump.sql gnukhata_db_1:/

# import data into the db from the file
docker exec -t gnukhata_db_1 psql -U gkadmin gkdata -f /gkdump.sql
```
