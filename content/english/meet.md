---
title: "Meeting Room"
date: 2023-08-30T20:33:18+05:30
draft: false
---

### Redirecting to the meeting room 🚀

<!-- jitsi call -->
<meta http-equiv="refresh" content="0; url=https://meet.fsci.in/gnukhata" />

<!--<div id="meet" class="text-center mt-2">-->
<!--</div>-->
<!--<script src='https://meet.jit.si/external_api.js'></script>-->
<!--<script>-->
<!--const domain = 'meet.fsci.in';-->
<!--const options = {-->
<!--    roomName: 'gnukhata',-->
<!--    width: "100%",-->
<!--    height: 800,-->
<!--    parentNode: document.querySelector('#meet')-->
<!--};-->
<!--const api = new JitsiMeetExternalAPI(domain, options);-->
<!--</script>-->
<!--<style>-->
<!--iframe {-->
<!--  aspect-ratio: 16/9;-->
<!--  width: 100%;-->
<!--}-->
</style>
