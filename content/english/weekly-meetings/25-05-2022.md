---
title: "Weekly Meeting: 25-05-2022"
date: 2022-05-25T12:16:20+05:30
draft: false
---

## Karthik

Dashboard

- Refactor code in tiles, Most valued Customer/Supplier
- API changes for dashboard for drilldown in tables

## Survesh

- GSTR-1 Json support

## GN

- Instead of archiving gkwebapp repo as a whole, Inspecting each issue & closing it is recommended

## VK

- Global INR formatting

## rest2e2

How shall we handle gkwebapp issues?
