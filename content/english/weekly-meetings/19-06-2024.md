---

title: "Weekly Meeting: 06-19-2024"
date: 2024-06-19T11:03:15+05:30
draft: false
---

## [Priyanka](https://gitlab.com/priyanka1992/)
 - gkapp#466 completed assigned to QA - fix:
    debit note/credit note/ rejection note issues
 - gkcore#465 completed assigned to QA - fix: debit/credit not showing for cash memo sales -feature request.
 - gkapp#494 completed  completed assigned to QA - Feature request: rejection note should show voucher details and hyperlink to voucher and vice versa
 

## [Kannan V M](https://gitlab.com/kannanvm)
Documented transactions section voucher entries - https://pad.nixnet.services/L4z7JKIoTguM7bD3-fhjbw.
Sat with QA team and Neeraj for issue organizing.
Fixed four issues.
    1) [Editing the Discount amount on business page gives 'Data Error'.](https://gitlab.com/gnukhata/gkapp/-/issues/539)
    2) [Calculate discount amount from discount percentage in business item creation screen.](https://gitlab.com/gnukhata/gkapp/-/issues/537)
    3) [Auto calculate Stock Value in business item creation screen.](https://gitlab.com/gnukhata/gkapp/-/issues/533)
    4) [Receipt voucher being created for purchase invoice.](https://gitlab.com/gnukhata/gkapp/-/issues/504)
Went through related issues for other assigned issues  and categorized them.

## [Ambady Anand S](https://gitlab.com/bady)

Updated gkcore README with instructions to fix a couple of issues with installation on Linux distros - [#890](https://gitlab.com/gnukhata/gkcore/-/issues/890), [#891](https://gitlab.com/gnukhata/gkcore/-/issues/891).
Investigated why gkcore setup fails with Python v3.12 and documented the findings along with a temporary workaround - [#745](https://gitlab.com/gnukhata/gkcore/-/issues/745)
Documented an issue along with a recommended fix regarding the deprecation of the current installation method using setup.py directly - [#893](https://gitlab.com/gnukhata/gkcore/-/issues/893)
Started testing and documenting gkcore manual installation steps (i.e. without docker. The gkutil.sh script used to setup Postgres failed to set necessary permissions, need to fix it): https://pad.nixnet.services/1QqRjRqGTamAFjzTQ4I1mw
Attended daily issue triage call starting from 14/06/2024 (Friday).
Gained access to the GNUKhata development server via VPN.
## [GN](https://gitlab.com/gnowgi)

## [VK](https://gitlab.com/bardwaj)

## [rest2e3](https://gitlab.com/rest2e3)
