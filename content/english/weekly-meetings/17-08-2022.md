---
title: "Weekly Meeting: 17-08-2022"
date: 2022-08-17T12:19:34+05:30
draft: false
---

## Karthik

- save report date & filters after drilldown in all reports

## Survesh

- test db migration code & create org workflow in progress...

## GN

- gst columns filter for view registers
- Plan for a beta release ASAP
- inspect the existing invoice charges PR & try to do it in our way

## VK

- follow up the user from Nepal & try to get some info on dev from there who can involve in adding support for their country

## rest2e2

- when drilling down the reports apply the same date filters
- stock on hand should point to registers
- other charges in invoices before the release
