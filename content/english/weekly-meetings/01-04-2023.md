---
title: "Weekly Meeting: 01-04-2023"
date: 2023-04-01T17:05:41+05:30
draft: false
---

## Karthik

- started working on openAPI spec
- migrated UOM component
- fix product/service update not updating the changed UOM

## Survesh

NIL

## GN

- prioritise the openAPI components with suggestions from R2

## VK

NIL

## rest2e2

ABSENT

## Rahul

- Learning the accounting & REST API skills
