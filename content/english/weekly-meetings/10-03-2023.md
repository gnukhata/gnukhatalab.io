---
title: "Weekly Meeting: 10-03-2023"
date: 2023-03-16T23:56:40+05:30
draft: false
---

## Karthik

- handle -ve balances for bank/cash balance
- fix gross/net trial balances not shown

## Survesh

- place of supply in invoice might be front-end issue

## GN

- Karthik should document all gnukhata info in a file for future team convenience
- Karthik should give access to Rahul to GNUKhata information so that he will have a brief overview of the project

## VK

- Welcome Rahul Zaveri to GNUKhata team.

## rest2e2

NIL
