---
title: "Weekly Meeting: 30-08-2023"
date: 2023-08-30T09:09:24+05:30
draft: false
---

## [Karthik](https://gitlab.com/kskarthik/)

gkapp

- 35ffba4b ci: disable ui_tests until QA team writes the plan
- 157aefdb fix(ProductRegister): navigation state not preserved. Closes #350
- bd2427df ci: add ui tests job
- 9142c2e4 refactor: handle ui scopes some user roles
- 1a256d5c tests: Updated the plan
- 509e58a8 Updated the test plan
- 41ad4460 Merge branch 'UI_Tests' into 'devel'
- c721b270 feat: UI tests

gkcore

- login api refactor, for QA team requirements
- discussed with R2 regarding P/L calculation errors

QA team

- gkcore cicd tests are taking a lot of time & fail. the team is working on the fix
- gkapp tests are added to the repo, but plan file is not ready.

## [Ankita](https://gitlab.com/Ankita_GNU)

- Working on P/L issues https://gitlab.com/gnukhata/gkcore/-/issues/675

## [GN](https://gitlab.com/gnowgi)

- Write pseudo code which helps understand the root of the issue

## [VK](https://gitlab.com/bardwaj)

- Same as above

## [rest2e3](https://gitlab.com/rest2e3)

Let's create a masked URL for our meeting room eg: `meet.gnukhata.org` which redirects to the actual url.
