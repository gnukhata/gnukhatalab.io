---
title: "Weekly Meeting: 07-09-2022"
date: 2022-09-07T12:15:09+05:30
draft: false
---

## Karthik

- navbar, about page refactor
- show FY info below orgname
- build docker image for gkcore stable branch

## Survesh

view, delete, reject, accept invites
fix bug in create new user and login

## GN

- no need of app version info in sidebar, just in about page is good enough
- add hint for username section in invite user

## VK

- what version mode will be used?
  A: We will follow GNOME's versioning scheme
- what if the user want to switch to another version of gnukhata
- Disable WIP parts of gkapp

## rest2e2

NIL
