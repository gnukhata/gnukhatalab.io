---
title: "Weekly Meeting: 26-10-2022"
date: 2022-10-26T12:40:27+05:30
draft: false
---

## Karthik

- several minor bug fixes in gkapp
- created the report bug component

## Survesh

- New fin year login after roll over
- Update HSN with debounce
- Remove no godown artifacts from UI
- Add Org address and state in create org form

## GN

- use different icon for workflow
- add color contrast for gnukhata forms in future
- for alpha it's closed testing with selected testers, for 30 days. Beta: public testing for 60 days.

## VK

- debounce time should be configurable
- select default godown as default

## rest2e2

- mv captcha from create org to create user
