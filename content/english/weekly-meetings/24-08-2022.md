---
title: "Weekly Meeting: 24-08-2022"
date: 2022-08-24T11:37:10+05:30
draft: false
---

## Karthik

- fix(gst/news): overflow text
- refactor(stockonhand): drill down to product register
- refactor(cashFlow): include date range in drill down
- feat(Registers): toggle b/w simple & expanded table views

## Survesh

ABSENT

- Fixed issues with create org API
- Invite user API
- Add user API
- set Password API

## GN

- set negative balance bg to red in stock on hand

## VK

NIL

## rest2e2

ABSENT
