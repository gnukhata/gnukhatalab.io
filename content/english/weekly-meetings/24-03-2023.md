---
title: "Weekly Meeting: 24-03-2023"
date: 2023-03-24T16:31:45+05:30
draft: false
---

## Karthik

- Fixed the place of supply issue in purchase invoice creation
- documentation updates for gkcore

## Rahul

- Learning the accounting concepts & REST API basics

## GN

ABSENT

## VK

NIL

## rest2e2

ABSENT
