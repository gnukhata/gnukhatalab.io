---
title: "Weekly Meeting: 29-04-2023"
date: 2023-04-29T17:08:25+05:30
draft: false
---

## Karthik

- add contributors guidelines for gkcore & gkapp
- new alpha release

## Survesh

ABSENT

## Ankita

ABSENT

- added dev setup docs for windows 11
- handle db url for windows platform in `gkcore_cli.py`

## GN

ABSENT

## VK

NIL

## rest2e3

NIL
