---

title: "Weekly Meeting: 04-03-2024"
date: 2024-04-03T11:03:15+05:30
draft: false
---

## [Priyanka](https://gitlab.com/priyanka1992/)
 - gkapp#889 completed assigned to QA - UI >> Sale Invoice >> Sale Invoice should have 'Mode Of Receipt' instead of 'Mode Of Payment
 - gkcore#442 completed assigned to QA - UI >> Purchase Invoice with Credit >> "Invoice Value in words" is not showing correct value.
 - gkcore#443 code complete - UI >> Transfer Note >> Transfer note should not allow to select Dispatch From and Dispatch To with Same Godown details
 - gkcore#440 completed assigned to QA - UI >> IFSC Code >> All pages should auto populate bank details along with IFSC Code, if Mode of Payment is Bank.
 - gkapp#437 completed assigned to QA - UI >> Stock on Hand >> The Product details are displayed for past and future year.
 - gkcore#421 completed assigned to QA - UI >> Purchase Order Or Sale Order >> On Purchase Order Details page, Sale Order is showing.


## [Ankita](https://gitlab.com/Ankita_GNU)

## [GN](https://gitlab.com/gnowgi)

## [VK](https://gitlab.com/bardwaj)

## [rest2e3](https://gitlab.com/rest2e3)
