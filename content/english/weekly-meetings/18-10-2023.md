---
title: "Weekly Meeting: 18-10-2023"
date: 2023-10-18T11:26:53+05:30
draft: false
---

## [Karthik](https://gitlab.com/kskarthik/)

1. Types of logging suggested: App logging & Activity logging
2. gkapp:

- 1d1d3e65 fix(InviteUser): properly reset the form. Closes #363
- b7ad71e6 chore(Dockerfile): bump node version to latest lts

3. gkcore:

- d9e16af ci: add warning in Dockerfile about the python version
- ed3235c wip: try fixing failing pserve in docker
- ac943c5 fix(setup): rm env variable for version

- use git lfs to host kt videos

## [Ankita](https://gitlab.com/Ankita_GNU)

1. Node.js 18.x installation completed. gkapp working fine.
2. gkapp issue created in Gitlab.
3. Continuing with Udemy course of Vue.js

## [Priyanka](https://gitlab.com/priyanka1992)

1. Listing the issues from the KT.
2. Two of them fixed, continuing with others.

## [GN](https://gitlab.com/gnowgi)

1. Logs should be the part of gkcore.
2. Use of decorators(Python) for generating Logs.
3. Discussion on time required for resolution of GST Issue.( Target is 4-6 months)

## [VK](https://gitlab.com/bardwaj)

1. Logs should be generated with a timestamp.
2. Invoking of log API should be done automatically.
3. Fix Profit/Loss and BalanceSheet then GST Issue.

## [rest2e3](https://gitlab.com/rest2e3)

1. Api for logging is present & log api should be invoked.
