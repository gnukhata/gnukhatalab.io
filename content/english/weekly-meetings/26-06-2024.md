---

title: "Weekly Meeting: 06-26-2024"
date: 2024-06-26T11:03:15+05:30
draft: false
---

## [Priyanka](https://gitlab.com/priyanka1992/)
 - gkapp#449 completed assigned to QA - fix:Enhancement >> Voucher>>Remove Edit and Cancel buttons for linked vouchers in Voucher module
 - gkcore#465 completed assigned to QA - UI >> Invoice >> Customer/Supplier Details Edit >> not allowing to save changes( Save Changes button is disabled) until GST is provided.
 - gkcore#161 completed assigned to QA - fix: Add Billwise adjustment to Invoice Profile Page
 - gkapp#147 completed assigned to QA - fix: show button to generate ledger report from contacts page
 - gkapp#494 completed - But as confirm with R2 rejection note is redundent so we need to hide this option in application.
 - Attended daily issue triage meetings.
 - Making documentation for Transaction

## [Kannan V M](https://gitlab.com/kannanvm)
Closed two issues - https://gitlab.com/gnukhata/gkapp/-/issues/301, https://gitlab.com/gnukhata/gkapp/-/issues/393
While bug fixing, faced few issues related to APIs
    1) There is no system for backend validation
    2) API logic is written inside a try-except block, making debugging difficult
    3) Database handling, mostly about tracking changes to existing fields
Did an R&D about handling back end validation, went through pyramid documentation and supporting libraries, there is pydantic, colander. Both have pros and cons. I'll add an issue with details about it. (https://docs.pylonsproject.org/projects/colander/en/latest/basics.html, https://docs.pylonsproject.org/projects/colander/en/latest/basics.html)
Going through Pyramid documentation to get an idea about how APIs are handled in pyramid, I'll add an issue with suggestions on how we can make API management easier once I complete it.
Going through SQLAlchemy documentation, the ORM used in the project. This is required to get an idea about current database management and also to setup automatic schema generation for back end validation.
Sat with priyanka to discuss about business logic documentation.
Sat with team for issue triage, went through all existing issues in gkapp

## [Ambady Anand S](https://gitlab.com/bady)

Fixed frontend state management issues related to place of supply and IGST vouchers getting wrongly created for non-IGST invoices. Related issues: [#214](https://gitlab.com/gnukhata/gkapp/-/issues/214), [#218](https://gitlab.com/gnukhata/gkapp/-/issues/218), [#286](https://gitlab.com/gnukhata/gkapp/-/issues/286), [#509](https://gitlab.com/gnukhata/gkapp/-/issues/509)
Completed documentation for gkcore manual installation. Added manual steps to configure postgres since the gkuitl.sh is not working as expected.
Found more issues with existing installation method using setup.py (Related issue: [#893](https://gitlab.com/gnukhata/gkcore/-/issues/893)). We're still using deprecated [egg package format](https://packaging.python.org/en/latest/discussions/package-formats/#what-about-eggs) and need to migrate to the [wheel format](https://packaging.python.org/en/latest/discussions/package-formats/#what-is-a-wheel). Need to update pyramid configuration for the same.
Discussed how to handle DB migration, validation errors and other exceptions with Kannan and Karthik.
Completed initial proofreading of the user documentation till reports section. Accounts section need some extensive review.
Investigated why pipeline failed for mkdocs used for documentation and shared the findings with Karthik.
Attended daily issue triage meetings.
## [GN](https://gitlab.com/gnowgi)

## [VK](https://gitlab.com/bardwaj)

## [rest2e3](https://gitlab.com/rest2e3)
