# Installation

## Linux

Linux users can install gnukhata simply by running this script inside their terminal.

```sh
wget -qO- https://gnukhata.org/install.sh | bash
```

The above command will install gnukhata using docker. If you would like to setup gnukhata without docker, check the developer documentation [here](developer-documentation/development_environment.md).
