# Setup Development environment

## Linux/Mac

### Prerequisites

- [docker](https://www.docker.com/)
- [docker-compose](https://docs.docker.com/compose/)
- [python](https://www.python.org/) (>= v3.8)
- [pip](https://pip.pypa.io/)

Configure system to run docker as non-root user as documented [here](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user).

```sh
# Install dependencies
# On Debian/Ubuntu distros
sudo apt install libpq-dev build-essential python3-dev python3-pip

# Clone the gkcore repo and switch to it
git clone https://gitlab.com/gnukhata/gkcore.git
cd gkcore/

# Create and activate python virtual environment
python3 -m venv gkenv
source gkenv/bin/activate
```

There are two ways to set up `gkcore`, via docker or manual. Before following
either of the methods below, you must complete the steps above.

### Via Docker

This is the most straight forward method to set up `gkcore`.


```sh
# install dependencies & initialize the application
./gkcore_cli.py init

# starts the dev server
./gkcore_cli.py serve
```

### Manual Way


```sh
# Install postgresql
# On Debian/Ubuntu
sudo apt install postgresql

# Start postgresql
sudo systemctl start postgresql.service

# Create database user
# The following creates a user with 'gkadmin' as both username and password.
# Make sure to replace it with a strong password.
sudo -u postgres psql -c "CREATE USER gkadmin WITH PASSWORD 'gkadmin';"

# Create database
sudo -u postgres psql -c "CREATE DATABASE gkdata;"

# Make gkadmin user as owner of the gkdata database
sudo -u postgres psql -c "ALTER DATABASE gkdata OWNER TO gkadmin;"

# Install gkcore along with its dependencies
python setup.py develop

# Export env variable required for database onnection in the following format
# Replace variables in angualr brackets with actual values
# postgresql://<username>:<password>@<host>:<port>/<database>
export GKCORE_DB_URL="postgres://gkadmin:gkadmin@127.0.0.1:5432/gkdata"

# Initialize database with the required data
python initdb.py

# Serve the application locally
pserve development.ini --reload
```

### Run

`gkcore` can now be accessed at `http://localhost:6543` from your web browser.

The API docs (swagger UI) can be accessed at `http://localhost:6543/docs/`.
