# Knowledge Base

## Accounting

These are useful video playlists for leaning accounting

- [Accounting for class 11](https://www.youtube.com/watch?v=IYTl9-T6K9k&list=PLVLoWQFkZbhWt47SsOOKEwuPms53Gdz5v) (English & Hindi)

- [Devika's Commerce & Management Academy](https://www.youtube.com/watch?v=8gvsTN10_tA&list=PLLhSIFfDZcUXEs3UhIv5tMn6AAU1GNnPr) (English)

### Print material

- [Accounting](https://www.icai.org/post.html?post_id=17757)

- [Accounting for Managerial Decisions](https://www.egyankosh.ac.in/handle/123456789/3158)

- [Corporate Accounting](https://www.egyankosh.ac.in/handle/123456789/73943)

- [Management Accounting](https://www.egyankosh.ac.in/handle/123456789/84017)

- [Accountancy-I](https://www.egyankosh.ac.in/handle/123456789/5006)

- [Accountancy II](https://www.egyankosh.ac.in/handle/123456789/5034)

- [Elements of Auditing](https://www.egyankosh.ac.in/handle/123456789/5030)

- [Principles and Practice of Accounting](https://www.icai.org/post.html?post_id=17825)

- [Financial Management](https://www.egyankosh.ac.in/handle/123456789/3161)

- [Financial accounting](https://openstax.org/details/books/principles-financial-accounting)

- [Fundamentals of Financial Management](https://www.egyankosh.ac.in/handle/123456789/80030)

### GST (India)

- [GST Cheatsheet](https://www.teachoo.com/subjects/tax-practical/gst/)

## Technical Knowledge

- [REST API Basics (video)](https://www.youtube.com/watch?v=WXsD0ZgxjRw)

- [Learn Python (video)](https://www.youtube.com/watch?v=_uQrJ0TkZlc&pp=ygULcHl0aG9uIG1vc2g%3D)

- [Docker (video)](https://www.youtube.com/watch?v=zJ6WbK9zFpI&list=PL2We04F3Y_42mOz2jsBqB_TOGIgaB8KkL)
