Business
=========

Business handles management of products and services of the organization. In GNUKhata, there's support for managing stock across multiple godowns, tax details and also has HSN Search to connect HSN/SAC code.

### Creating New Product

1. Select Business from Workflow section.
2. Click on create (+) button on bottom of list.
3. On the Business Item create page, select the nature of item between product and service, item name and price details.
4. In Stock section add unit of measurement. Note that this section will not be available for services.
5. To add new godowns click on "+Godown" button.
6. To add stock to existing godowns click on "+Stock" button. Select godown and stock quantity.
7. If your organization has GSTIN, GST details for the item can be updated from Tax section. If not skip to point 11.
8. HSN can be searched and code can be selected.
9. Applicable GST can be selected from the dropdown menu.
10. There's a toggle button to add GST rates based on date of applicability. Use if necessary. Select the GST rate and applicable from date.
11. Add CESS and CVAT.
12. Add state wise VAT taxes if any.
13. Click on reset if you want to clear the form and start over.
14. Click on Save and click ok to create the item.

### Updating and Deleting existing Business Items

1. Select the buisiness item from the list.
2. Click on Info / Price / Taxes section, update the relevant details.
3. To update godown wise opening stock, click on Godown Wise Opening section and add godown wise stock data. Godowns can be created from here. Note that this section will not be available for services.
4. Click on Save Changes button on top to save the updated stock details.
5. Click on delete product/service to delete them.

### Filtering Business Items

1. Click on the kebab menu in Business Items list.
2. Click on the funnel button and select required filters.
3. Click on reset to remove all filters.

### Downloading Business Items List

1. Click on the kebab menu in Business Items list.
2. Click on the download button to download as PDF.
3. Click on the spreadsheet button to download as XLSX.
