Purchase/Sales Order
=====================

Purchase Order: It is a document issued to vendors with information about product/services to be delivered, their price after negotiation, quantity, delivery date, and other details about the purchase. It is a legally binding document, which once seller agreed, is liable to deliver according to the Purchase Order. Buyer is also liable to pay the seller on mutually agreed Purchase Order. Invoices are usually generated with respect to Purchase Orders.

Sales Order: Once the seller agree upon the Purchase Order, seller issues the buyer a Sales Order with particulars about the purchase with respect to the Purchase Order.

### Creating Purchase/Sales Order

1. Go to Purchase/Sales Order under Transaction section under Workflow.
2. Click on create (+) button on bottom of list.
3. In the Create Purchase/Sales Order page, select between Sales and Purchase invoice.
4. Select Customer/Supplier. To add new Customer/Supplier click on the '+' button.
5. Update Customer/Supplier details if required using edit button.
6. Update Purchase/Sales Order Details section. Purchase/Sales Order number will be auto-generated and is editable. Place of supply and from godown are mandatory. GST nature (IGST, CGST, SGST) will be adjusted as per shipping address state and place of supply.
7. Use shipping address as same as billing address or update it to a new address.
8. Select between GST and VAT, default is GST.
9. Create item(s) for sales/purchase if you haven't already and add item(s) to items list.
10. Select quantity and product discount.
11. Click on the "-" on right to remove the item.
12. Click on the Round Off Total Value button if you want to round off the grand total and select payment details. If the mode of receipt/payment is bank, add account number and IFSC. Clicking on the search button next to the IFSC code will verify the same and can be used to autofill bank name and branch.
13. Update Transport Details. Specify if it is reverse charged.
14. Add comments.
15. 15. Click Reset if you want to reset the Purchase/Sales form and start over.
16. Click Create and select ok to generate Purchase/Sales Order.
17. Purchase/Sales Order previewe will be shown on successful creation.
18. Use print button to print the Purchase/Sales Order and close button to close the preview.
19. Click Back button to go back to the Purchase/Sales Order list.

### Filtering Purchase/Sales Order

1. Click on the kebab menu (Menu button with 3 vertical dots) in Purchase/Sales Order list.
2. Click on the funnel button and select required filters.
3. Click on reset to remove all filters.

### Downloading Purchase/Sales Order List

1. Click on the kebab menu in Purchase/Sales Order list.
2. Click on the download button.

### Printing invoice

1. Select Purchase/Sales Order from Purchase/Sales Order list.
2. Click on the print button on top right of Purchase/Sales Order.
