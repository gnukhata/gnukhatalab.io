Cash Memo
==========

Cash memo is a document used in business transactions to record cash sales or payments received. It typically includes details such as the date of the transaction, the amount received, the name of the buyer or customer, a description of the goods or services sold. They are usually used in retail stores, restaurants, and other businesses that accept cash payments.

Cash memo will not have IGST bills.

### Creating Cash Memo

1. Go to Cash Memo under Transaction section under Workflow.
2. Click on create (+) button on bottom of list.
4. Select buyer/seller. To add new buyer/seller click on the '+' button.
6. Update Cash Memo Details section. Cash Memo number will be auto-generated and is editable.
7. Select Mode of Receipt in Payment Details. If Bank is seleced, clicking on the search button next to the IFSC code will verify the same and can be used to autofill bank name and branch.
8. Select between GST and VAT, default is GST.
9. Create item(s) if you haven't already and add item(s) to item list.
10. Select quantity and product discount.
11. Click on the "-" on right to remove the item.
12. Toggle Round off to round off and select payment details. If the mode of receipt/payment is bank, add account number and IFSC. IFSC code can be searched to verify IFSC and auto populate bank, branch fields.
15. Click Reset if you want to reset the Cash Memo form and start over.
16. Click Create and select ok to generate Cash Memo.
17. Cash Memo preview will be shown on successful generation. Click on vouchers to see the transaction details and Delivery Note to see delivery note.
18. Use print button to print the Cash Memo and close button to close the preview.
19. Click on back button to go back to the Cash Memo list.

### Filtering Cash Memo

1. Click on the kebab menu in Cash Memo list.
2. Click on the funnel button and select required filters.
3. Click on reset to remove all filters.

### Downloading Cash Memo List

1. Click on the kebab menu in Cash Memo list.
2. Click on the download button.

### Printing Cash Memo

1. Select Cash Memo from Cash Memo list.
2. Click on the print button on top right of Cash Memo.
3. Select between Original, Duplicate and Triplicate to print Cash Memo.

### Other actions

1. View Voucher: To view Cash Memo related vouchers, click on view vouchers on top right.
2. View Delivery Note: To view Delivery Note, click on Delivery Note button on top right.
