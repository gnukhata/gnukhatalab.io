Debit/Credit Note
=================

Debit/Credit Notes are documents issued by seller to buyer to make adjustments to original invoice issued.

**Credit Note**: Credit Note is issued when the actual invoice value is less than the issued invoice. Multiple credit notes can be generated for an invoice. Seller will be liable to pay the difference in invoice value to buyer. Credit notes will reduce the tax liability on the seller if applicable.

Credit notes can be generated for reasons including,

- Return of defective goods or unsatisfactory service.
- Correction in invoice amount.
- Post sale discount.
- Post sale price reduction due to finalization of provisional assessment.
- Buyer receiving less than ordered.
- Tax charged in invoice is higher than tax payable for the supply.
- Change in Place of Supply.

**Debit Note**: Debit Note is issued when actual invoice value is higher than the issued invoice. Buyer will have to pay the additional money and seller will have additional tax liability.

Debit notes can be generated for reasons including,

- Correction in invoice amount.
- Tax charged in invoice is lower than tax payable for the supply.
- Buyer receiving more than ordered.
- Post sale price hike due to finalization of provisional assessment.


### Creating Debit/Credit Note

1. Go to Debit/Credit Note under Transaction section under Workflow.
2. Click on create (+) button on bottom of list.
3. In the Create Debit/Credit Note page, select between Sales and Purchase invoice.
4. Select the invoice.
5. Select Debit or Credit Note. Note number, Invoice details and Buyer/Seller details will be auto generated.
6. Update Note date, purpose and reference Note details.
7. Select GST/VAT.
8. Update the item details as required.
9. Add Note comments.
10. Click Reset if you want to reset the Debit/Credit Note form and start over.
11. Click Create and select ok to generate Credit/Debit Note.
12. Credit/Debit Note preview will be shown on successful creation. Click on Invoice to see the Invoice.
13. Use print button on top right to print the Credit/Debit Note and close button to close the preview.
14. Click Back button to go back to the Debit/Credit Note list.

### Filtering Credit/Debit Note

1. Click on the kebab menu (menu button with three vertical dots) in Credit/Debit Note list.
2. Click on the funnel button and select required filters.
3. Click on reset to remove all filters.

### Downloading Credit/Debit Note List

1. Click on the kebab menu in Credit/Debit Note list.
2. Click on the download button.

### Printing invoice

1. Select Credit/Debit Note from Credit/Debit Note list.
2. Click on the print button on top right of Credit/Debit Note.

### Other actions

1. View Invoice: To view related invoice, click on the View Invoice button on top right.

