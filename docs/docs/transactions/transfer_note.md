Transfer Note
==============

Transfer Note is a document for tracking the transport of stock between godowns. Transfer Note will include dispatch details and in GNUKhata you can approve Transfer Note with actual receipt date.

### Creating Transfer Note

1. Go to Transfer Note under Transaction section under Workflow.
2. Click on create (+) button on bottom of list.
3. In Transfer Note Details section Transfer Note number will be auto-generated and is editable.
4. Select Dispatch from, Disapatch to godowns and date of dispatch.
5. Update Transport Details section; Transport By, Date of Supply and Receipt Date are mandatory fields.
6. Add items and quantity to the list.
7. Click Reset if you want to reset the Transfer Note form and start over.
8. Click Create and select ok to generate Transfer Note.
9. Transfer Note preview will be shown on successful generation. Actual receipt date can be added and Transfer Note can be approved from here. 
10. Use print button on top right to print the Transfer Note and Close button to close the preview.
11. Click Back button to go back to the Transfer Note list.

### Approving Transfer Note

1. Select Transfer Note from Transfer Note list.
2. Update actual receipt date and click Approve button.

### Filtering Transfer Note

1. Click on the kebab menu in Transfer Note list.
2. Click on the funnel button and select required filters.
3. Click on reset to remove all filters.

### Downloading Transfer Note List

1. Click on the kebab menu in Transfer Note list.
2. Click on the download button to download as PDF.
3. Click on the spreadsheet button to download as XLSX.

### Printing Transfer Note

1. Select Transfer Note from Transfer Note list.
2. Click on the print button on top right of Transfer Note.
