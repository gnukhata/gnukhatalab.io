Delivery Note
==============

Delivery note contains details about the products that are shipped and details about transport. A copy of the Delivery note accompanies with the goods that are being transported. In GNUKhata, Delivery Notes are auto generated with invoices.

### Filtering Delivery Note list

1. Click on the kebab menu in the delivery note list.
2. Click the funnel button and select required filters.
3. Click on reset to remove all filters.

### Downloading Delivery Note list

1. Click on the kebab menu
2. Click the download button.

### Printing Delivery Note

1. Select Delivery Note from Delivery Note list.
2. Click on the print button on top right of Delivery Note.
3. Select between Original, Duplicate and Triplicate to print Delivery Note.

### Other actions

1. View invoice: To view Delivery Note related invoice, click on view invoice on top right.
