Rejection Note
===============

Rejection note is issued when the purchased/sold goods are defective or not up to expectation. In rejection note the quantity of returned goods are specified.

Rejection notes allows tracking returns/refunds for purchases/sales performed.


### Creating Rejection Note

1. Go to Rejection Note under Transaction section under Workflow.
2. Click on create (+) button on bottom of list.
3. In the Create Rejection Note page, select between Sales and Purchase Rejection.
4. Select the invoice.
5. Update Rejection Note Details section. Rejection Note number will be auto-generated and is editable. 
7. Select quantity of rejected items.
8. Add Rejection Note comments.
9. Click Reset if you want to reset the Rejection note form and start over.
10. Click Create and select ok to generate Rejection Note.
11. Rejection Note preview will be shown on successful generation. Click on invoice to see the related invoice.
12. Use print button on top right to print the Rejection Note and close button to close the preview.
13. Click Back button to go back to the Rejection note list.

### Filtering Rejection Notes

1. Click on the kebab menu (Menu button 3 vertical dots) in Rejection Note list.
2. Click on the funnel button and select required filters.
3. Click on reset to remove all filters.

### Downloading Rejection Note List

1. Click on the kebab menu in Rejection Note list.
2. Click on the download button.

### Printing Rejection Note

1. Select Rejection Note from Rejection Note list.
2. Click on the print button on top right of Rejection Note.

### Other actions

1. View Invoice: To view related invoice, click on view invoice on top right.
