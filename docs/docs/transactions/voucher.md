Voucher
========

Voucher is an internal document that is generated to track all the financial transactions in an organization. In GNUKhata the related financial transactions are linked and viewable from voucher detail.

GNUKhata supports following vouchers,

**Receipt Voucher**: Receipt voucher is to add payment receipts on an on-credit sales invoice.

**Payment Voucher**: Payment voucher is to add payments on an on-credit purchase invoice.

**Purchase Voucher**: Tracks cash and credit purchase of goods and raw materials.

**Sales Voucher**: Tracks cash and credit sales of goods.

**Contra Voucher**: Contra Voucher tracks transactions between cash related accounts. eg. Transferring funds from Cash in hand to Bank.

**Journal Voucher**: To add rectification entries.

**Credit Note Voucher**: To reduce the amount due from customer or for allowances.

**Debit Note Voucher**: To reduce the amount payable to supplier or for allowances.

**Sales Return Voucher**: Tracks Sales Returns.

**Purchase Return Voucher**: Tracks Purchase Returns.


### Creating Voucher

1. Go to Voucher under Transaction section under Workflow.
2. Click on create (+) button on bottom of list.
3. Select the Voucher type.
4. Select the related invoice if applicable.
5. Update Voucher date.
6. New account heads can be added by clicking the "+" button on Account row table head.
7. New rows for Crs and Drs can be created by clicking the "+" button on Cr and Dr columns respectively.
8. Select the account heads in each rows and update Cr/Dr values.
9. Add Comments.
10. Click Reset if you want to reset the Voucher form and start over.
11. Click Create and select ok to generate Voucher.
12. Click Back button to go back to the Voucher list.

### Filtering Voucher

1. Click on the kebab menu in Voucher list.
2. Click on the funnel button and select required filters.
3. Click on reset to remove all filters.

### Downloading Voucher List

1. Click on the kebab menu in Voucher list.
2. Click on the download button to download as PDF.

### Printing Transfer Note

1. Select Voucher Voucher Note list.
2. Click on the print button on top right of Voucher.

## References
[cleartax reference](https://cleartax.in/s/types-of-vouchers)
[icai reference](https://kb.icai.org/pdfs/PDFFile5b27976545f667.12985834.pdf)
