Purchase/Sale Invoice
=====================

Invoices are legally binding document between customer and supplier about price, quantity and payment conditions. GNUKhata supports generation of Sales Invoice and Purchase Invoice.

GNUKhata supports GST, VAT and CESS in the invoice. Corresponding tax value will be auto calculated for each taxes. GNUKhata also supports adding E-Way bill to the invoice. Discounts can be added per item (excluding tax).

Invoice related documents can be attached and uploaded as JPG, PNG images.

### Creating invoice

1. Go to Invoices under Transaction section under Workflow.
2. Click on create (+) button on bottom of list.
3. In the Create Invoice page, select between Sales and Purchase invoice.
4. Select buyer/seller. To add new buyer/seller click on the '+' button.
5. Update buyer/seller details if required using edit button.
6. Update Invoice Details section. Invoice number will be auto-generated and is editable. Place of supply and from godown are mandatory. GST nature (IGST, CGST, SGST) will be auto adjusted as with respect to place of supply.
7. Use shipping address as same as billing address or update it to a new address.
8. Select between GST and VAT, default is GST.
9. Create item(s) for sales/purchase if you haven't already and add item(s) to item list.
10. Select quantity and product discount.
11. Click on the "-" icon on right if you want to remove an item from the list.
  12. Click on the Round Off Total Value button if you want to round off the grand total and select payment details. If the mode of receipt/payment is bank, add account number and IFSC. Clicking on the search button next to the IFSC code will verify the same and can be used to autofill bank name and branch.
13. Update Transport Details, a Delivery Note will be auto generated with respect to the invoice. Specify if it is reverse charged.
14. Add comments and attach relevant documents in JPG/PNG format.
15. Click Reset if you want to reset the Invoice form and start over.
16. Click Create and select ok to generate invoice.
17. Invoice preview will be shown on successful invoice generation. Click on vouchers to see the transaction details.
18. Use print button on top right to print the invoice (Original, Duplicate or Triplicate) and close button to close the preview.
19. Click Back button to go back to the invoice list.

### Filtering Invoices

1. Click on the kebab menu (menu button with three vertical dots) in invoice list.
2. Click on the funnel button and select required filters.
3. Click on reset to remove all filters.

### Downloading Invoice List

1. Click on the kebab menu in invoice list.
2. Click on the download button.

### Printing invoice

1. Select invoice from invoice list.
2. Click on the print button on top right of Invoice.
3. Select between Original, Duplicate and Triplicate to print invoice.

### Other actions

1. View attachments and vouchers: To view invoice related attachments vouchers, click on view vouchers on top right and they will be shown under the invoice details.
2. Cancel Invoice: To cancel invoice, click on cancel invoice button on top right and click ok.

