Contact
========

Contact section maintains a list of customer and supplier contacts and their details.

### Creating New Contact

1. Select Contacts from Workflow section.
2. Click on create (+) button on bottom of list.
3. Select between Customer and Supplier.
4. GSTIN based autofilling of organization details is available. To use that, enter the GSTIN of the contact, click on "Validate & Autofill", enter the captcha and submit.
5. Name and State are compulsory.
6. To add contact details, toggle on Contact Details section.
7. To add bank details, toggle on Bank Details section. Bank details section support IFSC based bank detail search. To use that, enter the IFSC number and click search.
8. Click Reset if you want to clear the form and start over.
9. Click Save to create contact and press ok.

### Contact Updation and Deletion

1. Select the contact from contact list.
2. Click on Info / Address / Financial Details / Bnank Details and update the relevant details and click on Save Changes button on top.
3. To Delete contact, click on delete contact button.

### Other Actions

1. Create Voucher: Click on Create Voucher button on contact details.
2. Create Invoice: Click on Add Transaction button on contact details.

### Filtering Contacts

1. Click on the kebab menu in Contact list.
2. Click on the funnel button and select required filters.
3. Click on reset to remove all filters.

### Downloading Contact List

1. Click on the kebab menu in Contact list.
2. Click on the download button to download as PDF.
