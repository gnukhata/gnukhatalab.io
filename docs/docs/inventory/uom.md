Unit of Measurements
====================

Units are essential when it comes to defining quantities of a product. GNUKhata by default provides UQC (Unique Quantity Code) unit list. Custom units and sub-units can also be made and connected to UQCs.

UQC is a standard measuring quantity under the GST system. Product quantity represented in UQC is required in e-invoices, e-way bills and when filling the HSN summary in GSTR-1. In GNUKhata valid GST Units are marked in green and Custom Units are marked in yellow. UQC units are not editable in GNUKhata.


### Creating Units

1. Go to `Inventory > Unit of Measurements`.
2. Click on "Add Unit" button.
3. Add name, description and related UQC.
4. Add parent units and the conversion rates, if any.
5. Click in "Create New Unit" button to create unit.

### Editing and deleting a Unit

1. Go to `Inventory > Unit of Measurements`.
2. Search for the Unit on the top search bar.
3. Click on the unit's name to go to unit's page.
4. To update the unit, update changes and click "Update Unit".
5. To delete the unit, click on the "Delete Unit" button.
