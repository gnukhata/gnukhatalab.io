Godowns
========

GNUKhata has multi-godown support, so that you can distribute your inventory across multiple godowns.

### Creating Godown

1. Go to `Inventory > Godowns`.
2. Click on "Add Godown" button.
3. Enter Godown details. Name, state and address are compulsory.
4. Click reset if you want to reset the form and start over.
5. Click "Save" and press "OK" to save Godown.

### Godown Updation and Deletion

1. Select the godown from the godown list.
2. Update the necessary details and click on Update button.
3. To Delete godown, click on Delete button.

### Other Actions

1. Exporting godown list: Click on the spread sheet button on top right to download godown list in XLSX format.
