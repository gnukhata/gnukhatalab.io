Categories
===========

Categories, also known as Inventory Groups, can be useful when a group of products share similar specifications and taxes, like different models of same TV or different packaging of same soap. GNUKhata supports categorizing products with specifications and taxes.

### Creating new Category

1. Go to `Inventory > Categories`.
2. Click on "Add Category" button.
3. Enter category name and parent category if any.
4. To enter specification details toggle "Add Specification" and add details.
5. To enter tax specification details toggle "Add Tax" and add details.
6. Click reset to reset the form.
7. Click "Save" and press "OK" to save category.

### Editing and deleting a Category

1. Go to `Inventory > Categories`.
2. Search for the category on the top search bar.
3. To edit the category, click on the edit button against that category.
4. To delete the category, click on the delete button against that category.

### Other actions

1. Exporting Category list: Click on the spreadsheet button on top right to export Category list in XLSX format.
