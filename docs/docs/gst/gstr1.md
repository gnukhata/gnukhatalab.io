GSTR-1 Report
==============

GSTR-1 Report contains details about all sales and submitted monthly or quarterly. This return is filed to record all sales that are happened.

GNUKhata supports generating GSTR-1 reports for monthly and for custom periods. GNUKhata also supports downloading GSTR-1 reports in JSON format which can be uploaded to GST portal to file GSTR-1 report.

### Generating GSTR-1 Report

1. Go to `GST > R1 Report`.
2. Select period type and period.
3. Click Show to view GSTR-1 report.
4. To export the report in XLSX format, click on the "Spreadsheet" button on top right of report.
5. To copy the JSON string, click the copy button on the GSTR-1 JSON section.
This JSON string can be pasted at GST portal to submit GSTR-1 report.
6. To download the JSON string, click the download button on the GSTR-1 JSON section.
