GSTR-3B Report
==============

GSTR-3B is a self-declared summary return to declare the summary of GST liabilities for a particular tax period for the taxpayer. 

GNUKhata supports generating GSTR-3B reports for monthly and for custom periods. GNUKhata also supports downloading GSTR-3B reports in JSON format which can be uploaded to GST portal to file GSTR-3B report.

### Generating GSTR-3B Report

1. Go to `GST > 3B Report`.
2. Select period type and period.
3. Click Show to view GSTR-3B report.
4. To export the report in XLSX format, click on the "Spreadsheet" button on top right of report.
5. To copy the JSON string, click the copy button on the GSTR-3B JSON section.
This JSON string can be pasted at GST portal to submit GSTR-3B report.
6. To download the JSON string, click the download button on the GSTR-3B JSON section.
