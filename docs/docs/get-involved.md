# Get Involved

GNUKhata is a free and open source software. There are multiple ways in which you can contribute to the improvement by filing bugs, providing new ideas, translations, code changes.

## General support

- [Telegram Group](https://t.me/gnukhata)
- [Matrix](https://matrix.to/#/#gnukhata:poddery.com)
- [Mailing List](https://www.freelists.org/list/gnukhata-users)

## Developer Discussions

- [Telegram Group](https://t.me/gnukhata-devel)
- [Matrix](https://matrix.to/#/#gnukhata-devel:poddery.com)
- [Mailing List](https://www.freelists.org/list/gnukhata-devel)

## Weekly Meetings

The team meets Every **Wednesday at 11:30 AM (IST)** to discuss about development, features, outline plans for next release. Feel free to join us & suggest your ideas / comments / feedback.

More info: [https://gnukhata.org/weekly-meetings/](https://gnukhata.org/weekly-meetings/)
