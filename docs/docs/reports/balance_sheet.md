Balance Sheet
==============

Balance sheet gives an overview about an organization's capital, liabilities, properties and assets at a given point of time. In balance sheet, Capital and Liabilities should be equal to Property and Assets. 

In GNUKhata, balance sheet for a period can be generated.


### Generate Balance Sheet Statement

1. Go to `Reports > Balance Sheet`.
2. Give the opening date and to date.
3. Click on "Get Details".
4. To hide 0 value rows, toggle "Hide ₹0 rows" option at top right.
5. To print Balance Sheet, click on the print icon on top right.
6. To export to XLSX format, click on the funnel icon at top right.
