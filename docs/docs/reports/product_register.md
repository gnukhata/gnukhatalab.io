Product Register
================

Product register will show you the inward - outward flow of a product in a godown. In GNUKhata, you can filter the product movement list by transaction type and export them in XLSX format.

### List product movement in a Godown

1. Go to `Reports > Product Register`.
2. Enter product name, the period which you are tracking and the godown.
3. Click view to list product movement.
4. Transactions can be searched from top left search field of list.
5. Transaction type can be filtered using funnel icon on top right of list.
6. Product register can be exported to XLSX format using spreadsheet icon on top right of the list.
7. To go to connected transaction, click on the transaction number against the transaction.
