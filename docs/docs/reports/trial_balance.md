Trial Balance
==============

Trial balance is a report that lists the balances of all general ledger accounts of a organization at a certain point in time. In GNUKhata, you can preview trial balance in three modes,

- **Net**: The Net Trial Balance will provide the closing balances (or current balance as on selected end date)
- **Gross**: The Gross Trial Balance shows for each account the total Drs and Crs along with Closing Balances
- **Extended**: The extended version shows all gross & net trial balances with Opening Balances


### Actions

**Trial Balance for custom period**: Get trial balance for custom periods by adjusting from date and to date.
**Searching**: To search for entries in Trial Balance, use the search field at top left of the list.
**Changing balance type mode**: Click filter icon on top right of the list and select required mode.
**Exporting Trial Balance**: Click on the spreadsheet icon on top right of the list to export Trial Balance in XLSX format.
