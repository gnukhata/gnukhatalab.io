Stock on Hand
==============

In GNUKhata, you can see the godown-wise stock on hand to manage your inventory.

### Accessing Stock on Hand

1. Go to `Reports > Stock on Hand`.
2. Enter product name, godown name and date at which you want to see the stock on hand.
3. Use the search field on top of product list to search for products.
