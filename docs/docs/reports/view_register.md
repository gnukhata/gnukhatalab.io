View Registers
===============

You can see your consolidated sales and purchase register here. If your company is GST registered, you can see GST Split amounts for your sale and purchases in the expanded table.

### Accessing Sales/Purchase Register

1. Go to `Reports > View Registers`.
2. Select between Sale and Purchase.
3. Enter the period you want to look up.
4. Click on "Get Details" to view the register.
5. To search entries, use search bar on top left of the list.
6. To expand table with tax details, toggle the "Expanded Table" option on top right of the list.
