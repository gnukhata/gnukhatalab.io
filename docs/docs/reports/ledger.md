Ledger
======

Entries of a Ledger Account for custom period can be viewed here. You will be able to see transaction type, Cr/Dr balance and go to respective vouchers.

GNUKhata also supports monthly consolidated reports that shows Cr/Dr balances along with the number of Cr/Dr records.


### Viewing Ledger Account for a period

1. Go to `Records > Ledger`.
2. Select account name and period.
3. Click show.
4. To search ledger entries, use search field at top left of the list.
5. To filter use the funnel icon at top right of the list.
6. To export to XLSX format, use the spreadsheet icon at top right.


### Viewing Monthly Statistics

1. Go to `Records > Ledger`.
2. Select "Monthly Ledger".
3. Select account name and period.
4. Click show.
5. To export to XLSX format, use the spreadsheet icon at top right.
