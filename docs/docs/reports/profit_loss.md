Profit & Loss
==============

Profit & Loss statement can be used to get an overview about revenues, expenses, gross profit and net income.

GNUKhata supports the horizontal format for Profit and Loss statement. Closing stock in GNUKhata Profit & Loss report is calculated using FIFO algorithm. 

### Accessing Profit & Loss

1. Go to `Reports > Profit & Loss`.
2. Enter the preriod to which you want to see the Profit & Loss report.
3. Click "Get Details" to fetch Profit & Loss report for the period.

