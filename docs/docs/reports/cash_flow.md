Cash Flow
==========

Cash Flow Statement shows the cash inflows and outflows of a business. It can help assess the companies operations, where money is coming from and how money is spent.


### Accessing Cash Flow

1. Go to `Reports > Cash Flow`.
2. Enter the preriod to which you want to see the Cash Flow report.
3. Click "Get Details" to fetch Cash Flow report for the period.
