Help
=====

### About

Read about GNUKhata and its history.

### Website

Link to GNUKhata website; go to GNUKhata website to see latest news and posts from GNUKhata.

### Source Code

Link to GNUKhata GitLab repository; check out the latest changes to your favorite accounting software.

GNUKhata is a free software. You can access and review GNUKhata's source code. Consider contributing to GNUKhata.

### Report Bug

GNUKhata needs you to report bugs! Even if you do not know how to develop software, testing and reporting bugs is a way to contribute to GNUKhata.

### FAQ

GNUKhata has an intuitive interface, if you have still any doubts, go through our extensive FAQ.
