Close Books/Roll Over
======================

Periodicity concept is one of the important principles of Accounting. According to this concept accounts are to be prepared after every period. Books for a company is usually closed and rolled over after every financial year, starting from April 1 to March 31st of the following year.

In GNUKhata, books can be closed at the end of financial year. Once books closed, no financial transactions can be done in that organization for that year.

Once the books are closed for the financial year, you can roll over to new financial year. You can switch between financial year from user menu on top right.

### Closing books for the financial year

1. Make sure your records for the financial year are in order. Once the book is closed for the year, no adjustments can be made.
2. Click Close Books/Roll Over from Administration menu.
3. Select current financial year end date.
4. Click Close Books.

### Roll Over to new financial year

1. Close books for current financial year.
2. Select new financial year start date and end date.
3. Click roll over.

### Switching between financial years

1. Click on the "Switch FY" in the user menu at top right.
2. Select financial year.
