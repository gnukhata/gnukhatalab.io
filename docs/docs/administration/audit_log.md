Audit Log
==========

Audit Log tracks every activity in the application. In the Audit Log list, activities are listed with date time and user who performed the action. 


### Actions

Filtering: Activity for custom date range can be filtered by using the funnel icon on top right.
Searching: Activity can be searched using the search field on top left of the activity table.
