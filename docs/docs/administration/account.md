Accounts
=========

GNUKhata follows Double Entry Book keeping and keeps its accounting close to Generally Accepted Accounting Principles.

In GNUKhata, all accounts are categorized by groups and subgroups. There will be thirteen groups by default and no additional groups can be added. New subgroups can be added for further classification of accounts.  Groups and subgroups can not be deleted. This categorization helps generating reports for the business.

Some of the groups GNUKhata supports are,

#### Direct Income
Direct income handles income accounts that are directly related to operating activities. These affect the Gross Profit of the organization. GNUKhata opens a Profit & Loss Account under this group. Following accounts will come under this category,

- Sales accounts
- Less sales return accounts

#### Direct Expense
Direct expense handles expense accounts that are directly related to production or purchase of goods or services. These items will affect gross profit of the organization. Following accounts will come under this category,

- Purchase accounts
- less purchase return
- Carriage inward
- Factory expenses
- Freight
- Import Duty
- Octroi
- Wages

#### Indirect Income
Indirect income handles income accounts that are not directly related to sales of goods and services. These items will reflect on the net profit of the organization. Following accounts will come under this category,

- Discount received
- Round off received
- Bad debt received
- Commission received
- Income from investment
- Rent received
- Interest received

#### Indirect Expense
Indirect expense handles expense accounts that are not directly related to production or purchase of goods or services. These items will reflect on the net profit of the organization. Following accounts will come under this category,

- Discount allowed
- Professional fees
- Round off paid
- Bonus
- Depreciation expense
- Office expense
- Bad debt
- Commission allowed
- Insurance
- Export duty
- Interest on loan
- Legal expenses
- Audit fees
- Postage and telegram
- Printing and stationary
- Salary
 

#### Fixed assets
Fixed Assets handle assets that are owned by the organization with intention of being used to produce goods and services and held across multiple financial years. These are not held for sale in the normal course of business. Following accounts will come under this category,

- Building account
- Furniture account
- Plant & Machinery accounts

#### Investments
Investments handles accounts that related to long term investments of a business. Following accounts will come under this category,

- Fixed Deposits
- Investments in businesses
- Secured/unsecured loans given to others
- Shares/mutual funds held 

#### Current Assets
Current assets handle asset accounts that usually sold, consumed or exhausted within a year. Following accounts will come under this category,

- Cash in hand
- Bank accounts
- Sundry debtors (All customer debtor accounts)
- Closing stock
- Stock at the beginning
- Short term advances given to employees

#### Current Liabilities
Current Liabilities handles accounts that related to short term obligation or liabilities of a business. Following accounts will come under this category,

- Sundry creditors for purchase
- Accounts for PF, ESI, TDS dues
- Provisions for income tax
- Interest payable
- Notes payable
- overdraft
- cash credit loan
- expenses payable
- others payable

#### Loans (Non-Current Liabilities)
Non-Current Liabilities handles accounts that related to long term obligation or liabilities of a business. Following accounts will come under this category,

- Loans

#### Capital
These represent the capital liablility of a business. Following accounts will come under this category,

- Reserves and Surplus
- Capital Account
- Drawings

### Creating an Account

1. Click on Accounts under Administration menu.
2. On Accounts page click on card header of relevant group. This will give a list of sub group.
3. Click on the "+" button on the relevant sub group's card header.
4. Enter account name and add opening  balance if applicable.
5. Click the toggle button to add account to a newly created subgroup.
6. Click reset to reset the form.
7. Click on save and press ok to create account.

### Other actions

1. Filtering accounts: Click on the funnel icon to filter accounts by groups and subgroups.
2. Exporting date: Click on the spreadsheet button to export data in XLSX format.



### references
- [Fundamental Analysis - Zerodha](https://zerodha.com/varsity/module/fundamental-analysis/)
- [Understanding Balance Sheet Statement (Part 1) - Zerodha](https://zerodha.com/varsity/chapter/understanding-balance-sheet-statement-part-1/)
- [Understanding the P&L Statement (Part 1) - Zerodha](https://zerodha.com/varsity/chapter/understanding-pl-statement-part1/)
- [Liabilities on Balance Sheet - Toppr](https://www.toppr.com/guides/fundamentals-of-accounting/final-accounts/liabilities-on-balance-sheet/)
- [Assets on Balance Sheet - Toppr](https://www.toppr.com/guides/fundamentals-of-accounting/final-accounts/assets-on-balance-sheet/)
- [Profit And Loss Account - Toppr](https://www.toppr.com/guides/principles-and-practices-of-accounting/final-accounts-for-sole-proprietors-non-manufacturing/profit-and-loss-account/)
- [Trading Account - Toppr](https://www.toppr.com/guides/principles-and-practices-of-accounting/final-accounts-for-sole-proprietors-non-manufacturing/trading-account/)
- [Assets and Liabilities - Groww](https://groww.in/p/assets-and-liabilities)
