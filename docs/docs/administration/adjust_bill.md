Adjust Bills
============

In credit sales and credit purchases, outstanding invoices can be adjusted with vouchers in Adjust Bills section.


### Adjusting Outstanding Invoices.

1. Click on Adjust Bills section under Administration menu.
2. In Billwise Accounting section select customer or supplier and from the dropdown menu select the customer/supplier.
3. Select already created payment/receipt voucher from the voucher dropdown on top right.
4. New vouchers can be created clicking "Add Voucher" button from bottom left.
5. Uncleared invoices can be adjusted to the amount of selected voucher. Multiple invoices can be cleared by this method in one go. Once invoice adjustment amounts are added to the table, click the "Adjust" button and press "ok".
