Manage Users
=============

GNUKhata has mult-user support with multiple user roles, enabling collaborative work while dividing work between employees. GNUKhata's role-based access control, allows administrators to assign specific roles to users, so they will have access only to their relevant sections. 

GNUKhata has the following user roles,

- Admin
- Manager
- Operator
- Internal Auditor
- Godown In charge

GNUKhata supports user access across organizations. Users can be invited, given a role or removed from an organization.


### Creating/Inviting a User

1. Select Manage Users from Administrative section in sidebar.
2. Click on the "Invite User" button on top.
3. To invite an existing user, enter the user name and click validate. If there's already an existing user with that name, a tick mark will be shown at end of the name input field. You can invite them by selecting a user role and clicking the "Invite User" button.
4. To create a new user, click on validate and select the checkbox asking to create new user.
5. Add password and security question. Security question can be used in case if you forgot your password. Remember to use a not so easy to guess secure password to protect your organization's accounting.
6. Click the "Create New User" button to create new user.

Invited/newly created users will be able to approve their invitation when they login to their account.

### Other Actions

1. Removing user from organization: Click on the "x" button against the user in the users list.
2. Deleting invitation: Click on the "bin" button on Invitations list.
3. Download user list: Click on spreadsheet button on top right to download user list in XLSX format.
