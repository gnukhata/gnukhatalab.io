Bank Reconciliation
====================

To assure the bank transactions are in order, every transaction in the book connected to bank has to have a respective entry in the bank statement. In GNUKhata you can compare your bank statements with the vouchers relating to each bank account. You can see the consolidated comparison in the reconciliation statement and correct them to validate all your bank transactions.

### Clearing vouchers

1. Click on the Bank Reconciliation section in Administration menu.
2. Select the bank account name which you like to compare.
3. Select the period and click get details. All uncleared vouchers will be listed below.
4. For each voucher, enter the respective clearance date in the bank statement. Add memo if required.
5. Click save button to clear the voucher.
6. Cleared vouchers can be updated from Cleared Vouchers section, which you can select by clicking Cleared Vouchers radio button.
7. Consolidated Reconciliation Statement can be seen in Statement section, which can be selected by clicking Statement radio button.
