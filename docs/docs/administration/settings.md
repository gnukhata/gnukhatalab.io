Settings
=========

## General

### Date Format

Default date format can be selected from here. GNUKhata supports three different date formats, `dd-mm-yyyy`, `mm-dd-yyyy` and `yyyy-mm-dd`.

### App Language

Default app language can be selected from here.

Right now GNUKhata supports only English, more translation efforts are welcome.

## Transaction

### Payment Mode

Default payment mode can be selected from here. The currently available options are bank, cash and credit.

### Tax Mode

Default Tax Mode can be selected from here.

GNUKhata supports GST, VAT and GST+VAT modes.

### Default Godown

You can select your default godown for products from here.

### Use Customer / Supplier ledgers to track transactions

If this option is enbaled, individual accounts will be created for each customer, rather than using single sales/purchase accounts.

## Organization

Organization details can be edited from here. GNUKhata supports autofilling organization details from GSTIN.

To save changes, click on "Save Changes" button. To delete a organization, click "Delete Organization" button.

### General

You can add organization name, website and email here. You can also upload organization logo, which will be visible on application navbar.

### Contact Details

You can add contact details of your organization here.

### Bank Details

Enter you bank details here. You can autofill Bank and Branch details by using IFSC code search feature.

### Tax details

You can add GSTIN and PAN from here. To autofil organization data from GSTIN, click on Validate & Autofill button under GSTIN field, fill the captcha and submit.

## Manage Users

Users can be invited and assigned user roles here. Existing users can be removed and also sent invitations can be revoked.

To see more details see Manage Users [TODO: Connect to Manage User page]

## Import Data

You can import data from other GNUKhata instances and Tally. Currently GNUKhata supports XLSX (Legacy) and JSON imports.

## Export Data

You can export GNUKhata data in XLSX (Legacy) and JSON format. The current exported spreadsheet contains only the accounts and vouchers data.
