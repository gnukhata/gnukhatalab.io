Budget
=======

To plan for the future, to set targets and trajectory for progress and performance of a business, Budgeting is a necessity. This can be done by increasing the revenue or by decreasing the expenses.

In GNUKhata, budgets can be created to set targets for Cash Flow and Profit & Loss. We can set revenue, expense targets to custom period of time.

### Creating Budget

1. Click on Budget under the Administration menu.
2. Select "+Add" button on the top.
3. Select between Cash (Flow) and Profit & Loss. 
4. Enter name of the budget.
5. Enter budget amounts in Budget Amount section. Inflow and outflow accounts will be listed for Cash Budget and income and expense accounts will be listed for Profit & Loss.
6. Check the summary on the left.
7. Click reset to reset the form.
8. Click Save and then press ok to save the Budget entry.

### View, Edit and Delete Budget

1. In Budget page, select budget type.
2. Select budget from the drop down menu.
3. Click edit and delete to edit/delete the budget.
4. By default budget detail will show only consolidated date for groups. Click on the expand button to show all accounts.
5. Click on print button to print the Budget.
6. Click on the spreadsheet button to export it in XLSX format.
