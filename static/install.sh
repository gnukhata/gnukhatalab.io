#!/bin/bash
# Linux installer script for GNUKhata
# License: GPLv3
# Authors: Karthik <kskarthik@disroot.org>
# TODO:
# 0.Test all supported distros
# 1. Add support for new distros
# 2. Add Uninstall logic

set -e

# distro dependencies
debian_deps=(docker.io docker-compose)
suse_deps=(docker docker-compose)

compose_file='https://gitlab.com/gnukhata/build/-/raw/master/docker-compose.yml?ref_type=heads'
# work_path is user's home directory
work_path=~
work_dir='gnukhata'

prepare() {

	mkdir "$work_path"/"$work_dir";
	cd "$work_path"/"$work_dir" || exit 1; 
	wget "$compose_file" -O docker-compose.yml;

}

launch() {

	cd $work_path/$work_dir || exit 1;
	sudo docker-compose up -d;
	echo "GNUKhata is now Installed!"
	echo "Open http://localhost:2020 in your web browser"

}
# debian/ubuntu
if command -v apt &>/dev/null; then
	
	echo "Detected OS: Debian/Ubuntu"

	echo "Installing dependencies ..."
	
	sudo apt install -y "${debian_deps[@]}";

	# enable/start docker services
	sudo systemctl enable docker.service;
	sudo systemctl enable containerd.service;

	sudo systemctl start containerd.service;
	sudo systemctl start docker.service;

	prepare && launch
	exit
fi

# openSUSE
if command -v zyppr &>/dev/null; then
	
	echo "Detected OS: openSUSE"

	echo "Installing dependencies ..."
	sudo zypper install -y "${suse_deps[@]}" ;
	
	# enable/start docker services
	sudo systemctl enable docker.service;
	sudo systemctl enable docker.socket;
	sudo systemctl enable containerd.service;

	sudo systemctl start containerd.service;
	sudo systemctl start docker.service;
	sudo systemctl start docker.socket;

	prepare && launch;
	exit
fi

echo "Your OS is not yet supported ☹ " &&\
echo "If you wish to contribute, Please refer: https://gitlab.com/gnukhata/gnukhata.gitlab.io/-/blob/master/static/install.sh?ref_type=heads" &&
exit 1
