#!/bin/bash

today=$(date +%d-%m-%Y)

if [[ "$1" != "" ]]; then
	today=$1
fi

hugo new weekly-meetings/$today.md

$EDITOR content/english/weekly-meetings/$today.md
